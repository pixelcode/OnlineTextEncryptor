# Online Text Encryptor
This is a website implementation of the Firefox addon **[Text Encryptor](https://codeberg.org/pixelcode/TextEncryptor)**. It encrypts your confidential text using AES, the most secure encryption algorithm in the world. No-one is able to crack it within a lifetime – not even the police or NSA!

Download the original Firefox addon:

[![](https://codeberg.org/pixelcode/TextEncryptor/raw/branch/master/get-the-addon.png)](https://addons.mozilla.org/firefox/addon/text-encryptor)

<br>

### Use cases
- encrypt your private notes
- send your banking details unmonitored to a friend who uses WhatsApp only sigh
- store your passwords on your PC without exposing them to hackers

### Online Text Encryptor
- doesn’t track you
- doesn’t store, send or upload your inputs anywhere
- is ad-free
- is FOSS (free and open source software)
<br><br><br>

![](https://codeberg.org/pixelcode/TextEncryptor/raw/branch/master/Header.png)
<br><br><br>

# Screenshot
![](https://codeberg.org/pixelcode/OnlineTextEncryptor/raw/branch/master/Screenshot%20EN%20light-mode%20full-page.png)

<br>

<!--
![](https://codeberg.org/pixelcode/OnlineTextEncryptor/raw/branch/master/Screenshot%20EN%20light-mode.png)
![](https://codeberg.org/pixelcode/OnlineTextEncryptor/raw/branch/master/Screenshot%20EN%20dark-mode.png)
![](https://codeberg.org/pixelcode/OnlineTextEncryptor/raw/branch/master/Screenshot%20DE%20light-mode.png)
![](https://codeberg.org/pixelcode/OnlineTextEncryptor/raw/branch/master/Screenshot%20EN%20light-mode%20mobile.png) -->
<br><br>

# Demo
You may try out a demo version of **Online Text Encryptor** on [pixelcode.codeberg.page/text-encryptor](https://pixelcode.codeberg.page/text-encryptor).
<br><br>

# Installation

1. [Download](https://codeberg.org/pixelcode/OnlineTextEncryptor/archive/master.zip) this repo or ```git clone https://codeberg.org/pixelcode/OnlineTextEncryptor```

2. Upload the downloaded repository folder to your webserver, for example via FTP. Your **Online Text Encryptor** instance will be available at ```http://example.com/OnlineTextEncryptor/```.

**No actual installation, back-end code or Node.js required 🥳** 
<br><br>

# Demo on local machine
If you just want to try **Online Text Encryptor** out on your private machine download this repo as stated above. Open the ```index.html``` file in a browser such as Firefox/Tor, Chromium or Opera. Most browsers will show you a working demo of your website thanks to the fact that **Online Text Encryptor** doesn't need any server-side code or backend.
<br><br>

# Documentation and security
Please refer to [the original repository](https://codeberg.org/pixelcode/TextEncryptor).
<br><br>

# Translation and i18n
**Online Text Encryptor** is available in English and German. It reads the user's preferred language and redirects him to the respective translation automatically.

If you want to use **Online Text Encryptor** in another language than English and German please refer to **Text Encryptor**'s **[translation instructions](https://codeberg.org/pixelcode/TextEncryptor/src/branch/master/TRANSLATE.md)**. Please consider contributing 😊

<br>

# Licence
**Text Encryptor** uses **[CrypoJS'](https://github.com/brix/crypto-js)** aes.js created by [Evan Vosberg (brix)](https://github.com/brix). It may be used according to the [MIT license](https://opensource.org/licenses/MIT). Thank you, Evan!

**Text Enryptor** uses **[jQuery](https://jquery.com)**.

**Text Encryptor** uses **[Font Awesome](https://fontawesome.com)** icons. These may be used according to the [Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/) license.

You may use **Online Text Encryptor's** source code according to the [For Good Eyes Only Licence v0.2](https://codeberg.org/pixelcode/OnlineTextEncryptor/src/branch/master/For%20Good%20Eyes%20Only%20Licence%20v0.2.pdf).

All versions of Online Text Encryptor are licensed under v0.2, which replaces v0.1 under which Online Text Encryptor was previously licensed. v0.1 is therefore void for any versions of Online Text Encryptor.

![](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/branch/master/banner/Banner%20250.png)